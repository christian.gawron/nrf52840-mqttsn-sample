#include "sdk_common.h"
#if NRF_MODULE_ENABLED(NRF_LOG)
#include "nrf_log_backend_interface.h"
#include "nrf_log_str_formatter.h"
#include "nrf_log_internal.h"

#include "log_syslog.h"


#include <openthread/udp.h>
#include <openthread/instance.h>
#include <openthread/message.h>

#define BUFFER_SIZE 512
static uint8_t buffer[BUFFER_SIZE];

static const uint16_t SYSLOG_PORT = 514;
static const uint8_t SYSLOG_HOST[16] = {
    0xfd, 0x00, 0xca, 0xfe, 0xba, 0xbe, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03};

/**@brief OpenThread transport instance. */
static otInstance *log_instance;

/**@brief OpenThread UDP socket. */
static otUdpSocket log_socket;

NRF_LOG_BACKEND_DEF(syslog_log_backend, nrf_log_backend_syslog_api, NULL);

void receiveCB(void *aContext, otMessage *aMessage, const otMessageInfo *aMessageInfo)
{
    OT_UNUSED_VARIABLE(aContext);
    OT_UNUSED_VARIABLE(aMessage);
    OT_UNUSED_VARIABLE(aMessageInfo);
}

uint32_t log_syslog_init(void *instance)
{
    log_instance = (otInstance *)instance;
    otError err_code;
    otSockAddr addr;

    memset(&addr, 0, sizeof(addr));

    err_code = otUdpOpen(log_instance, &log_socket, receiveCB, 0);
    if (err_code == OT_ERROR_NONE)
    {
        addr.mPort = SYSLOG_PORT;
        err_code = otUdpBind(&log_socket, &addr);
    }

    int32_t backend_id = nrf_log_backend_add(&syslog_log_backend, NRF_LOG_SEVERITY_DEBUG);
    ASSERT(backend_id >= 0);
    nrf_log_backend_enable(&syslog_log_backend);

    return (err_code == OT_ERROR_NONE) ? NRF_SUCCESS : NRF_ERROR_INTERNAL;
}

static void log_syslog_put(void const * p_context, char const *p_buffer, size_t len)
{
    uint16_t pri = 16 * 8 + 6;
    uint32_t err_code = NRF_ERROR_INVALID_STATE;
    otMessage *p_msg = NULL;
    otUdpSocket *p_socket = &log_socket;
    char *sendbuf = malloc(len + 64);

    do
    {
        if (sendbuf == NULL)
        {
            err_code = NRF_ERROR_NO_MEM;
            break;
        }

        // IETF Doc: https://tools.ietf.org/html/rfc5424
        // BSD Doc: https://tools.ietf.org/html/rfc3164
        sprintf(sendbuf, "<%d>1 - %s %s - - - \xEF\xBB\xBF ", pri, "nrf", "sample");
        strncat(sendbuf, p_buffer, len);

        p_msg = otUdpNewMessage(log_instance, NULL);
        if (p_msg == NULL)
        {
            err_code = NRF_ERROR_NO_MEM;
            break;
        }

        if (otMessageAppend(p_msg, sendbuf, strlen(sendbuf)) != OT_ERROR_NONE)
        {
            err_code = NRF_ERROR_INTERNAL;
            break;
        }
        otMessageInfo msg_info;

        memset(&msg_info, 0, sizeof(msg_info));
        msg_info.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;
        msg_info.mPeerPort = SYSLOG_PORT;

        memcpy(msg_info.mPeerAddr.mFields.m8, SYSLOG_HOST, OT_IP6_ADDRESS_SIZE);

        if (otUdpSend(p_socket, p_msg, &msg_info) != OT_ERROR_NONE)
        {
            err_code = NRF_ERROR_INTERNAL;
            break;
        }

        err_code = NRF_SUCCESS;

    } while (0);

    if ((p_msg != NULL) && (err_code != NRF_SUCCESS))
    {
        otMessageFree(p_msg);
    }
    if (sendbuf != NULL)
    {
        free(sendbuf);
    }

    // return err_code;
}


uint32_t syslog_printf(const char *template, ...)
{
    uint32_t err_code = NRF_ERROR_INVALID_STATE;
    char message[256];
    memset(message, 0, 255);
    va_list ap;
    va_start(ap, template);
    vsnprintf(message, 255, template, ap);
    va_end(ap);

    log_syslog_put(NULL, message, strlen(message));
    err_code = NRF_SUCCESS;
    return err_code;
}

static void nrf_log_backend_syslog_put(nrf_log_backend_t const *p_backend,
                                       nrf_log_entry_t *p_msg)
{
    nrf_memobj_get(p_msg);

    nrf_fprintf_ctx_t fprintf_ctx = {
        .p_io_buffer = (char *)buffer,
        .io_buffer_size = BUFFER_SIZE,
        .io_buffer_cnt = 0,
        .auto_flush = false,
        .p_user_ctx = NULL,
        .fwrite = log_syslog_put};

    nrf_log_str_formatter_entry_params_t params;

    nrf_log_header_t header;
    size_t memobj_offset = 0;

    nrf_memobj_read(p_msg, &header, HEADER_SIZE * sizeof(uint32_t), memobj_offset);
    memobj_offset = HEADER_SIZE * sizeof(uint32_t);

    params.timestamp = header.timestamp;
    params.module_id = header.module_id;
    params.dropped = header.dropped;
    params.use_colors = NRF_LOG_USES_COLORS;

    /*lint -save -e438*/
    if (header.base.generic.type == HEADER_TYPE_STD)
    {
        char const *p_log_str = (char const *)((uint32_t)header.base.std.addr);
        params.severity = (nrf_log_severity_t)header.base.std.severity;
        uint32_t nargs = header.base.std.nargs;
        uint32_t args[NRF_LOG_MAX_NUM_OF_ARGS];

        nrf_memobj_read(p_msg, args, nargs * sizeof(uint32_t), memobj_offset);
        memobj_offset += (nargs * sizeof(uint32_t));

        nrf_log_std_entry_process(p_log_str,
                                  args,
                                  nargs,
                                  &params,
                                  &fprintf_ctx);
    }
    else if (header.base.generic.type == HEADER_TYPE_HEXDUMP)
    {
        uint32_t data_len = header.base.hexdump.len;
        params.severity = (nrf_log_severity_t)header.base.hexdump.severity;
        uint8_t data_buf[8];
        uint32_t chunk_len;
        do
        {
            chunk_len = sizeof(data_buf) > data_len ? data_len : sizeof(data_buf);
            nrf_memobj_read(p_msg, data_buf, chunk_len, memobj_offset);
            memobj_offset += chunk_len;
            data_len -= chunk_len;

            nrf_log_hexdump_entry_process(data_buf,
                                          chunk_len,
                                          &params,
                                          &fprintf_ctx);
        } while (data_len > 0);
    }
    nrf_memobj_put(p_msg);
}

static void nrf_log_backend_syslog_flush(nrf_log_backend_t const *p_backend)
{
}

static void nrf_log_backend_syslog_panic_set(nrf_log_backend_t const *p_backend)
{
}

const nrf_log_backend_api_t nrf_log_backend_syslog_api = {
    .put = nrf_log_backend_syslog_put,
    .flush = nrf_log_backend_syslog_flush,
    .panic_set = nrf_log_backend_syslog_panic_set,
};

#endif