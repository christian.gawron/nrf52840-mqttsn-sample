#ifndef LOG_BACKEND_SYSLOG_H
#define LOG_BACKEND_SYSLOG_H

#include "nrf_log.h"

#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

extern const nrf_log_backend_api_t nrf_log_backend_syslog_api;

uint32_t log_syslog_init(void *otInstance);
uint32_t syslog_printf(const char *template, ...);

#endif